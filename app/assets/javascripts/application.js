// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(window).load(function() {
    fetch_free_API($('#post_breed_url').val());
    $('#post_breed_url').on('change', function(){
        var url = $(this).val();
        fetch_free_API(url);
    });
});

function fetch_free_API(url){
    $.ajax({
        type: "GET",
        url: url,
        dataType: "json", // Set the data type so jQuery can parse it for you
        success: function (data) {
            console.log(data);
            var status = data["status"];
            var pic_url = data["message"];
            if(status == 'success')
                $('.dog img').attr('src', pic_url);
        }
    });
}
