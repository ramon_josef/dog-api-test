class DogBreedFetcher
  attr_reader :breed

  def initialize(name=nil)
    # @name  = breed || "random"
    # @breed = Breed.find_or_initialize_by(name: name)

    Breed.destroy_all
    lists = update_breeds["message"]
    for item in lists do
      if(item[1].length == 0)
        name = item[0].capitalize
        pic_url = "https://dog.ceo/api/breed/" + item[0] + "/images/random"
        @breed = Breed.new(name: name, pic_url: pic_url)
        @breed.save
      else
        for item_1 in item[1] do
          name = item[0].capitalize + "/" + item_1.capitalize
          pic_url = "https://dog.ceo/api/breed/" + item[0] + "/" + item_1 + "/images/random"
          @breed = Breed.new(name: name, pic_url: pic_url)
          @breed.save
        end
      end
    end
  end

  def fetch

    @breed = Breed.all

    return @breed

    # if @breed.pic_url.present?
    #   @breed.pic_url = fetch_info["message"]
    #   @breed.save && @breed
    # end
  end

  def self.fetch(name=nil)
    name ||= "random"
    DogBreedFetcher.new(name).fetch
  end

private
  def fetch_info
    begin
      JSON.parse(RestClient.get("https://dog.ceo/api/breeds/image/#{ @name }").body)
    rescue Object => e
      default_body
    end
  end

  def default_body
    {
      "status"  => "success",
      "message" => "https://images.dog.ceo/breeds/cattledog-australian/IMG_2432.jpg"
    }
  end

  def update_breeds
    JSON.parse(RestClient.get("https://dog.ceo/api/breeds/list/all").body)
  end

end
